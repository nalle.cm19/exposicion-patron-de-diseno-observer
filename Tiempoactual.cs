﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observador
{
    //SE realiza herencia entre la clase y la interfaz//
    class Tiempoactual : IObserver
    {
        //Se declaran las variables mediante el encapsulado
        public string Ciudad { get; set; }
        public string TiempoActual { get; set; }
        public string TiempoActual2 { get; set; }
        public string TiempoActual3 { get; set; }

        public Tiempoactual(String nombre)
        {
            Ciudad = nombre;
        }
        //Se llama al método//
        public void Actualizar(ISubject subject)
        {
            if (subject is InformacionClimatica informacion)
            {
                //Se agrega mensaje que visualizará el usuario
                Console.WriteLine("La temperarura actual de la ciudad de {0} es de {1} grados Celsius", Ciudad, informacion.Temperatura);
                Console.WriteLine();
            }
        }
    }
}
        