﻿using System;

namespace Observador
{
    class Program
    {
        static void Main(string[] args)
        {
            //Este patrón define una dependencia entre objetos
            //Esto se realiza a través de su estructura: Sujeto-->Observador-->IObservador-->ISujeto
            Console.WriteLine("");
            Console.WriteLine("                                    Bienvenido a su programa de control climático                                    ");
            Console.WriteLine("Aquí podrá mantenerse informado sobre las actualizaciones del clima con la ciudad y la temperatura");
            Console.WriteLine("                                                  **App climática**                                              ");
            Console.WriteLine("");
            //Se declara variables//
            InformacionClimatica informacion = new InformacionClimatica();

            //Para proceder a agregar nuevos datos es necesario llamar al método//
            Tiempoactual TiempoActual = new Tiempoactual("Manta");
            informacion.Agregar(TiempoActual);

            Tiempoactual TiempoActual2 = new Tiempoactual("Montecristi");
            informacion.Agregar(TiempoActual2);

            Tiempoactual TiempoActual3 = new Tiempoactual("Portoviejo");
            informacion.Agregar(TiempoActual3);
            //Se agregan las variables que en este caso serán Temperatura
            informacion.Temperatura = 30.3f;
            informacion.Temperatura = 35.5f;
            informacion.Temperatura = 29.6f;
            Console.ReadLine();
        }
    }
}
