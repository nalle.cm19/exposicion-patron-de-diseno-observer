﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observador
{
        //Herencia de la interfaz
    class InformacionClimatica : ISubject
    {
        //Realización de lista de la interfaz Observer
        private List<IObserver> _observadores;
        //Se declara la variable
        public float Temperatura
        {
            get { return _temperatura; }
            set
            {
                _temperatura = value;
                Notificar();
            }
        }
        private float _temperatura;

        public InformacionClimatica()
        {
            _observadores = new List<IObserver>();
        }
        public float temperatura()
        {
            return this.Temperatura;
        }
        //Permitirá agregar nueva información
        public void Agregar(IObserver observador)
        {
            _observadores.Add(observador);
        }
        //La opcion de notificar permite revisar y avisar los cambios que se hayan realizado, procede a darle paso al método actualizar
        public void Notificar()
        {
            _observadores.ForEach(o =>
           {
               o.Actualizar(this);
           });
        }
        // Se puede realizar modificaciones como desactivar subcripción
        public void Quitar(IObserver usuario)
        {
            _observadores.Remove((IObserver)_observadores);
        }    
    }
}
