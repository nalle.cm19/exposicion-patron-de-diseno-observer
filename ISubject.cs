﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observador
{
    public interface ISubject
    {
        //El sujeto del patron de diseño utiliza tres métodos Agregar, quitar y notificar//
        void Agregar(IObserver observer);
        void Quitar(IObserver observer);
        void Notificar();
    }
}
