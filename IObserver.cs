﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observador
{
    public interface IObserver
    {
        //El observador cuenta con un método que se relaciona con el sujeto, de esa forma se mantiene informado y procede actualizar
        void Actualizar(ISubject subject);
    }
}